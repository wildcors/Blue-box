var gulp = require('gulp'),
    runSequence = require('run-sequence').use(gulp),
    htmlmin = require('gulp-htmlmin'),
    autoprefixer = require('gulp-autoprefixer'),
    cssnano = require('gulp-cssnano'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    concatStyle = require('gulp-concat-css'),
    sass = require('gulp-sass'),
    del = require('del'),
    browserSync = require('browser-sync');

gulp.task('html', function() {
    return gulp.src('./*.html')
        .pipe(htmlmin({
            removeComments: true,
            ignoreCustomFragments: [/(\<|\>)/g]
        }))
        .pipe(gulp.dest('assets/'));
});
gulp.task('style', function() {
    return gulp.src(['source/styles/**/*.css', 'source/styles/**/*.scss'])
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9'))
        .pipe(concatStyle('styles.css'))
        .pipe(cssnano())
        .pipe(gulp.dest('assets/css'))
});
gulp.task('script', function () {
   return gulp.src('source/js/**/*.js')
       .pipe(uglify())
       .pipe(gulp.dest('./assets/js'))
});
gulp.task('img-min', function () {
   return gulp.src('source/images/**/*')
       .pipe(imagemin({ optimizationLevel: 3, progressive: true}))
       .pipe(gulp.dest('./assets/images'))
});
gulp.task('clean', function() {
    return del(['./assets/style/*', './assets/js/*', './assets/images/*', './assets/*.html']);
});
gulp.task('browser-sync', function () {
   var files = [
      'assets/**/*.html',
      'assets/css/**/*.css',
      'assets/images/**/*.png',
      'assets/js/**/*.js'
   ];

   browserSync.init(files, {
      server: {
         baseDir: './'
      }
   });
});
gulp.task('watch', ['browser-sync'], function () {
  gulp.watch('./*.html', ['html']);
  gulp.watch('source/images/**/**.*', ['img-min']);
  gulp.watch('source/js/*.js', ['script']);
  gulp.watch('source/styles/*.css', ['style']);
  gulp.watch('source/styles/*.scss', ['style']);
})
gulp.task('default', ['clean'], function () {
   runSequence('img-min', 'html', 'style', 'script', 'watch');
});