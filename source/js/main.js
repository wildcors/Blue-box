'use strict';
$(document).ready(function(){

    $(".owl-carousel.top").owlCarousel({
        loop: true,
        autoplay: true,
        autoplayTimeout: 4000,
        smartSpeed: 500,
        margin: 10,
        responsiveClass: true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:3
            }
        }
    });

    $(".owl-carousel.slider-cli").owlCarousel({
        loop: true,
        margin: 10,
        responsiveClass: true,
        dots: false,
        nav: true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:3
            }
        }
    });
    window.onload = function () {
        var preloader = document.querySelector('#page-preloader');

        preloader.style.opacity = 0;

        setTimeout(
            function () {
                preloader.style.display = 'none';
            },
            333);
    };

    var slideout = new Slideout({
        'panel': document.getElementById('panel'),
        'menu': document.getElementById('menu'),
        'padding': 256,
        'tolerance': 70
    });

    document.querySelector('.toggle-button').addEventListener('click', function() {
        slideout.toggle();
    });

    var $li = $('.img-list').find('> li'),
        $links = $li.find('> a'),
        $lightbox = $('.lightbox'),
        $next = $('.next'),
        $prev = $('.prev'),
        $overlay = $('.overlay'),
        liIndex,
        targetImg;

    //preload images
    // var imgSources = [
    //     './images/gallery/img-0-large.jpg',
    //     './images/gallery/img-1-large.jpg',
    //     './images/gallery/img-2-large.jpg',
    //     './images/gallery/img-3-large.jpg'
    // ];

    // var imgs = [];
    // for (var i = 0; i < imgSources.length; i++) {
    //     imgs[i] = new Image();
    //     imgs[i].src = imgSources[i];
    // }

    // pop-up
    function replaceImg(src) {
        $lightbox.find('img').attr('src', src);
    }

    function getHref(index) {
        return $li.eq(index).find('>a').attr('href');
    }

    function closeLigtbox() {
        $lightbox.fadeOut();
    }

    $overlay.click(closeLigtbox);

    $links.click(function(e) {
        e.preventDefault();
        targetImg = $(this).attr('href');
        liIndex = $(this).parent().index();
        replaceImg(targetImg);
        $lightbox.css({"opacity":"1","display":"flex"});
    });

    $next.click( function() {
        if ( (liIndex + 1) < $li.length ) {
            targetImg = getHref(liIndex + 1);
            liIndex ++;
        } else {
            targetImg = getHref(0);
            liIndex = 0;
        }
        replaceImg(targetImg);
    });

    $prev.click( function() {
        if ( (liIndex) > 0 ) {
            targetImg = getHref(liIndex - 1);
            liIndex --;
        } else {
            targetImg = getHref($li.length - 1);
            liIndex = $li.length - 1;
        }
        replaceImg(targetImg);
    });

    var Regexp = {
        search : /^[а-яА-ЯёЁa-zA-Z0-9/\s/]+$/,
        name : /^[а-яА-ЯёЁa-zA-Z0-9-\ ]{1,20}$/,
        mail : /^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/,
        message : /^[а-яА-ЯёЁa-zA-Z0-9-_ )(\.,:;""]{1,300}$/
    };



    (function (form) {
        var fields = {
            name : form.querySelector(".name"),
            mail : form.querySelector(".mail"),
            message : form.querySelector(".message")
        };

        var submit = document.querySelector(".submit"),
            v = 0;

        //Form checking
        function checkField (field) {

            if (field.value != field.defaultValue) {
                if (Regexp[field.name].test(field.value)) {
                    v++;
                    field.style.border = "2px solid #58d99c";
                } else {
                    field.style.border = "2px solid #f4776d";
                    form.querySelector(".error-message").style.opacity = "1";
                }
            } else {
                field.style.border = "2px solid #f6ab25";
            }
        };

        function startCheck() {
            v = 0;
            for (var prop in fields) {
                checkField(fields[prop]);
            }
            if (v == 3) {
                form.querySelector(".error-message").style.opacity = "0";
                form.querySelector(".sending").style.opacity = "1";
            }
        };

        submit.addEventListener("click", startCheck);

    }(document.querySelector(".contact-form")));

    //AttachFileChecking
    (function (input) {
        var button = input.querySelector(".file"),
            fileName = document.createElement("div");

        fileName.className = "file-name";

        var fileTypes = ["pdf", "jpeg", "jpg", "png"],
            errorMessage = "File format only: pdf, jpeg/jpg, png";

        function checkFileType (file) {

            var typeOfFile = "",
                dot = file.indexOf(".") + 1,
                result;

            for (var i = dot; i < file.length; i++) {
                typeOfFile += "" + file[i];
            }
            for (var j = 0; j < fileTypes.length; j++) {

                if (typeOfFile == fileTypes[j]) {
                    return true;
                } else {
                    result = false;
                }
            }
            return result;
        }

        function getFileName (value) {


            var onlyName = "",
                numb = button.value.lastIndexOf("\\") + 1;

            for (var i = numb; i < value.length; i++) {
                onlyName += "" + value[i];
            }
            return onlyName;
        }

        function showFileName() {

            if (fileName.innerHTML == button.value || fileName.innerHTML == fileName.defaultValue) {
                return;
            } else if (button.value != "") {

                fileName.innerHTML = button.value;

                switch (checkFileType(button.value)) {
                    case true:
                        fileName.style.color = "#5bc187";
                        fileName.innerHTML = getFileName(button.value);
                        break;

                    case false:
                        button.value = "";
                        delete button.files;
                        fileName.style.color = "#c61118";
                        fileName.style.textAlign = "right";
                        fileName.innerHTML = errorMessage;
                        break;

                    default:
                        console.log("check showFile");
                        break;
                }
                input.parentNode.appendChild(fileName);
            }
        }

        input.addEventListener("change", showFileName);
    }(document.querySelector(".attech-container")));

});